<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 26/07/2018
 * Time: 22:58
 */

    session_start();
    require_once("model/Departamento.php");

    $oDepartamento = new Departamento();
    $cleiton ="";

    if ($_POST['txtNome']<>"" && strlen($_POST['txtNome'])<>0){
        $oDepartamento->setNome(addslashes($_POST['txtNome']));
    }else{
        $cleiton .= "Nome dpto. Inválido...";
    }

    if($_POST['txtGestor']<>"" && strlen($_POST['txtGestor'])<>0){
        $oDepartamento->setNomeGestor(addslashes($_POST['txtGestor']));
    }else{
        $cleiton .= " |Nome Gestor Inválido...";
    }

    if($_POST['txtCoor'] <> "" && strlen($_POST['txtCoor'])<>0){
        $oDepartamento->setNomeCoordenador(addslashes($_POST['txtCoor']));
    }else{
        $cleiton .= " |Nome do Coordenador inválido...";
    }

    try{
        if($oDepartamento->insert()){
            header("location:departamentos.php?erro=2");
        }else{
            header("location:departamentos.php?erro=1");
        }

    }catch (Exception $e){
        $cleiton .= " |Houve um problema para gravar... verifique! |".$e->getMessage();
    }




?>