<?php

session_start();
require_once("model/Origem.php");
require_once("model/functions.php");

$idOrigem = validaEntrada($_POST['sSistema'], 1);

echo informacoesGerais($idOrigem);

//region Funções
function informacoesGerais($idOrigem)
{
    $str = "";
    $oOrigem = new Origem();
    foreach ($oOrigem->retOrigemInformacoesGerais($idOrigem) as $key => $valor) {
        $str .= "<div class='row'>";
        $str .= "<div class='col-md-12'>";
        $str .= "<div class='card' style='width: 18rem;'>";
        $str .= "<ul class='list-group list-group-flush'>";
        $str .= "<li class='list-group-item'><b>Fornecedor:</b> $valor->nomefornecedor</li>";
        $str .= "<li class='list-group-item'><b>Backup:</b> $valor->formatobackup</li>";
        $str .= "<li class='list-group-item'><b>Destino:</b> $valor->nomedestino</li>";
        $str .= "</ul>";
        $str .= "</div>";
        $str .= "</div>";
        $str .= "</div>";
    }
    return $str;
}

//endregion

//region Validações

function validaEntrada($prValor, $prTipo)
{
    if ($prTipo == 0) {
        if (isset($prValor) && $prValor <> "") {
            $valorReturn = $prValor;
        } else {
            $valorReturn = "";
        }
    } else if ($prTipo == 1) {
        if (isset($prValor) && $prValor > 0) {
            $valorReturn = $prValor;
        } else {
            $valorReturn = 0;
        }
    } else {
        $valorReturn = null;
    }
    logBusca($_SESSION['idusuario'], $prValor);
    return $valorReturn;
}

//endregion

?>