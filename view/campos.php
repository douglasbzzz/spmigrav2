<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 22/06/2018
 * Time: 22:23
 */

session_start();
$erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
require_once("../model/Destino.php");
require_once("../model/Modulo.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>SpMigrações - Campos Migráveis</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../assets/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/geral.css"/>
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon-32x32.png">
</head>
<body>
<?php include "../header.php"; ?>
<div class="container">
    <div class="mb-1 mt-3 col-md-8 offset-md-2">
        <button id="btnNovo" type="button" class="btn btn-primary" data-toggle="modal" data-target="#mdFormCampos">
            <i class="fas fa-plus"></i> Novo Campo
        </button>
    </div>
    <div class="modal" id="mdFormCampos">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cadastro de Campos</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="frmEnviar">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" name="txtId" placeholder="ID" id="txtId" readonly/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" name="txtNomeCampo" id="txtNomeCampo"
                                placeholder="Nome do Campo (db's internos)" required/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" name="txtApelidoCampo" id="txtApelidoCampo"
                                placeholder="Alias do campo (descrição amigável)" required/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 form-inline">
                            <?php
                            $oDestino = new Destino();
                            $oModulo = new Modulo();
                            echo "<select class='form-control col-md-6' name='sDestino' id='sDestino' required>";
                            echo "<option value='0'>Selecione um Destino...</option>";

                            foreach ($oDestino->findAll() as $key => $valor) {
                                echo "<option value='" . $valor->id . "'>" . $valor->nome . "</option>";
                            }

                            echo "</select>";
                            echo "<select class='form-control col-md-6' name='sModulo' id='sModulo' required>";
                            echo "<option value='0'>Selecione o Módulo...</option>";

                            foreach ($oModulo->findAll() as $key => $valor) {
                                echo "<option value='" . $valor->id . "'>" . $valor->nome . "</option>";
                            }
                            echo "</select>";
                            ?>

                            <?php
                            if ($erro == 1) {
                                echo '<font color="#FF0000">Problema para salvar Campo, Verifique!<br/></font>';
                            } elseif ($erro == 2) {
                                echo '<font color="#008000">Campo Salvo com Sucesso!<br/></font>';
                            }
                            ?>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <div class="mr-auto">
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i> Sair</button>
                        </div>
                        <div class="ml-auto">
                            <button id="btnSalvar" type="button" class="btn btn-outline-success">
                                <i class="fas fa-save"></i> Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2">
        <div class="form-row">
            <form id="frmEnviarFiltro">
                <div class="form-group col-md-12">
                    <h3>Campos Cadastrados</h3>
                </div>
                <div class="form-group col-md-12">
                    <h6>Filtros</h3>
                </div>
                <div class="form-row" style="margin-left: 20px;">
                    <div class="form-group col-md-12 form-inline">
                        <?php
                            $oDestino = new Destino();
                            $oModulo = new Modulo();
                            echo "<select class='form-control' style='width: 200px !important;' name='sDestinoFiltro' id='sDestinoFiltro' required>";
                            echo "<option value='0'>Selecione um Destino...</option>";

                            foreach ($oDestino->findAll() as $key => $valor) {
                                echo "<option value='" . $valor->id . "'>" . $valor->nome . "</option>";
                            }

                            echo "</select>";
                            echo "<select class='form-control' style='width: 200px !important;' name='sModuloFiltro' id='sModuloFiltro' required>";
                            echo "<option value='0'>Selecione o Módulo...</option>";

                            foreach ($oModulo->findAll() as $key => $valor) {
                                echo "<option value='" . $valor->id . "'>" . $valor->nome . "</option>";
                            }
                            echo "</select>";
                        ?>
                    </div>
                </div>
            </form>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12" id="dtCampos">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../assets/jquery-3.3.1.js"></script>
<script type="text/javascript" src="../assets/bootstrap.js"></script>
<script type="text/javascript" src="../assets/sweetalert2.all.min.js"></script>
<script type="text/javascript">
        
    reloadGrid();

    $(document).ready(function () {

        //Carrega combo Modulos
        $('#sDestino').click(function () {
            if ($('#sDestino').val() > 0) {
                $.ajax({
                    url: '../preencheComboModulo.php',
                    method: 'post',
                    data: $('#frmEnviar').serialize(),
                    success: function (data) {
                        $('#sModulo').html(data);
                    },
                    error: function (data) {
                        $('#sModulo').html(data);
                    }
                })
            }
        });

        $('#sDestinoFiltro').click(function () {
            $.ajax({
                url: '../../controller/Campo/preencheComboModulo.php',
                method: 'post',
                data: $('#frmEnviarFiltro').serialize(),
                success: function (data) {
                    $('#sModuloFiltro').html(data);
                    reloadGrid();
                },
                error: function (data) {
                    $('#sModuloFiltro').html(data);
                    reloadGrid();
                }
            });
        });

        $('#sModuloFiltro').click(function () {
            reloadGrid();
        });

        //Novo
        $(this).on("click", "#btnNovo", function() {
            $('#txtId').val('');
            $('#txtNomeCampo').val('');
            $('#txtApelidoCampo').val('');
            $('#sDestino').val(0);
            $('#sModulo').val(0);
        });

        //Salvar
        $(this).on("click", "#btnSalvar", function() {
            $.ajax({
                url: '/controller/Campo/save.php',
                method: 'post',
                data: $('#frmEnviar').serialize(),
                success: function (data) {
                    var result = JSON.parse(data);
                    if(!result.error){
                        swal({
                            type: 'success',
                            title: 'Sucesso!',
                            text: result.msg
                        });
                        reloadGrid();
                        $('#mdFormCampos').modal('toggle');
                        $('#txtId').val('');
                        $('#txtNomeCampo').val('');
                        $('#txtApelidoCampo').val('');
                        $('#sDestino').val(0);
                        $('#sModulo').val(0);
                    } else {
                        swal({
                            type: 'error',
                            title: 'Erro!',
                            text: result.msg
                        });
                    }
                }
            });
        });

        //Editar
        $(this).on("click", "#btnEditar", function() {
            $.ajax({
                url: '/controller/Campo/getById.php',
                method: 'get',
                data: {id: $(this).val()},
                success: function (data) {
                    var result = JSON.parse(data);
                    $('#txtId').val(result.id);
                    $('#txtNomeCampo').val(result.nomecampo);
                    $('#txtApelidoCampo').val(result.apelido);
                    $('#sDestino').val(result.iddestino);
                    $('#sModulo').val(result.idmodulo);
                }
            });
        });

        //Excluir
        $(this).on("click", "#btnExcluir", function() {
            swal({
                title: 'Atenção!',
                text: "Você deseja mesmo deletar este registro?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '/controller/Campo/delete.php',
                        method: 'delete',
                        data: {id: $(this).val()},
                        success: function (data) {
                            var result = JSON.parse(data);
                            if(!result.error){
                                swal('Sucesso!',
                                    result.msg,
                                    'success'
                                );
                                reloadGrid();
                            } else {
                                swal('Erro!',
                                    result.msg,
                                    'error'
                                );
                            }
                        }
                    });
                }
            })
        });
    });

    function reloadGrid() {
        $.ajax({
            url: '/controller/Campo/getAll.php',
            method: 'get',
            data: {sDestinoFiltro: $('#sDestinoFiltro').val(), sModuloFiltro: $('#sModuloFiltro').val()},
            success: function (data) {
                $("#dtCampos").html(data);
            }
        });
    }
</script>

</body>
</html>