<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>SpMigrações - Destinos</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="../assets/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/geral.css"/>
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon-32x32.png">
    <link rel="stylesheet" href="../assets/fonts/fontawesome-free-5.5.0-web/css/all.css">
</head>
<body>
<?php include "../header.php"; ?>
<div class="container">
    <div class="mb-1 mt-3 col-md-8 offset-md-2">
        <button id="btnNovo" type="button" class="btn btn-primary" data-toggle="modal" data-target="#mdFormDestino">
            <i class="fas fa-plus"></i> Novo Destino
        </button>
    </div>
    <div class="modal" id="mdFormDestino">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Cadastro de Destinos</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="frmEnviar">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" name="txtId" placeholder="ID" id="txtId" readonly/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" name="txtNome" placeholder="Nome Destino" id="txtNome" required/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="mr-auto">
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i> Sair</button>
                        </div>
                        <div class="ml-auto">
                            <button id="btnSalvar" type="button" class="btn btn-outline-success">
                                <i class="fas fa-save"></i> Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2">
        <div class="form-row">
            <div class="form-group col-md-12">
                <h3>Destinos Cadastrados</h3>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12" id="dtDestinos">

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../assets/jquery-3.3.1.js"></script>
<script type="text/javascript" src="../assets/bootstrap.js"></script>
<script type="text/javascript" src="../assets/sweetalert2.all.min.js"></script>
<script>

    $(document).ready(function () {

        reloadGrid();

        //Novo
        $(this).on("click", "#btnNovo", function() {
            $('#txtId').val('');
            $('#txtNome').val('');
        });

        //Salvar
        $(this).on("click", "#btnSalvar", function() {
            $.ajax({
                url: '/controller/Destino/save.php',
                method: 'post',
                data: $('#frmEnviar').serialize(),
                success: function (data) {
                    var result = JSON.parse(data);
                    if(!result.error){
                        swal({
                            type: 'success',
                            title: 'Sucesso!',
                            text: result.msg
                        });
                        reloadGrid();
                        $( '#mdFormDestino' ).modal('toggle');
                        $('#txtId').val('');
                        $('#txtNome').val('');
                    } else {
                        swal({
                            type: 'error',
                            title: 'Erro!',
                            text: result.msg
                        });
                    }
                }
            });
        });

        //Editar
        $(this).on("click", "#btnEditar", function() {
            $.ajax({
                url: '/controller/Destino/getById.php',
                method: 'get',
                data: {id: $(this).val()},
                success: function (data) {
                    var result = JSON.parse(data);
                    $('#txtId').val(result.id);
                    $('#txtNome').val(result.nome);
                }
            });
        });

        //Excluir
        $(this).on("click", "#btnExcluir", function() {
            swal({
                title: 'Atenção!',
                text: "Você deseja mesmo deletar este registro?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '/controller/Destino/delete.php',
                        method: 'delete',
                        data: {id: $(this).val()},
                        success: function (data) {
                            var result = JSON.parse(data);
                            if(!result.error){
                                swal('Sucesso!',
                                    result.msg,
                                    'success'
                                );
                                reloadGrid();
                            } else {
                                swal('Erro!',
                                    result.msg,
                                    'error'
                                );
                            }
                        }
                    });
                }
            })
        });

    });

    function reloadGrid() {
        $.ajax({
            url: '/controller/Destino/getAll.php',
            method: 'get',
            success: function (data) {
                $("#dtDestinos").html(data);
            }
        });
    }

</script>
</body>
</html>
