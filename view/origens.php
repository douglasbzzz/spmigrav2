<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 22/06/2018
 * Time: 22:23
 */
session_start();
$erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
require_once("../model/Destino.php");
require_once("../model/Origem.php");

?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>SpMigrações - Origens</title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="../assets/bootstrap.css" />
        <link rel="stylesheet" href="../assets/geral.css" />
        <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32x32.png">
    </head>

    <body>
        <?php include "../header.php"; ?>
        <div class="container">
            <div class="mb-1 mt-3 col-md-8 offset-md-2">
                <button id="btnNovo" type="button" class="btn btn-primary" data-toggle="modal" data-target="#mdFormOrigem">
                    <i class="fas fa-plus"></i> Nova Origem
                </button>
            </div>
            <div class="modal" id="mdFormOrigem">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Cadastro de Origens</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form id="frmEnviar" action="origemController.php" method="post">
                            <div class="modal-body">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="txtId" placeholder="ID" id="txtId" readonly/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="txtNome" id="txtNome" placeholder="Nome Sistema Origem" required/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="txtFornecedor" id="txtFornecedor" placeholder="Nome do Fornecedor" required/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="txtFormatoBackup" id="txtFormatoBackup" placeholder="Formato ou extensão do Backup" required/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <?php
                                $oDestino = new Destino();
                                echo "<select class='form-control' name='sDestino' id='sDestino' required>";
                                echo "<option value='0'>Selecione um Destino...</option>";

                                foreach ($oDestino->findAll() as $key => $valor){
                                    echo "<option value='".$valor->id."'>".$valor->nome."</option>";
                                }

                                echo "</select>";
                                ?>

                                            <?php
                                if ($erro == 1) {
                                    echo '<font color="#FF0000">Problema para salvar sistema de Origem, Verifique!<br/></font>';
                                }elseif($erro == 2){
                                    echo '<font color="#008000">Sistema de Origem Salvo com Sucesso!<br/></font>';
                                }
                                ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="modal-footer">
                            <div class="mr-auto">
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i> Sair</button>
                            </div>
                            <div class="ml-auto">
                                <button id="btnSalvar" type="button" class="btn btn-outline-success">
                                    <i class="fas fa-save"></i> Salvar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Origens Cadastradas</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12" id="dtOrigem">

                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../assets/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="../assets/bootstrap.js"></script>
        <script type="text/javascript" src="../assets/sweetalert2.all.min.js"></script>
        <script>

            $(document).ready(function () {

                reloadGrid();

                //Novo
                $(this).on("click", "#btnNovo", function() {
                    $('#txtId').val('');
                    $('#txtNome').val('');
                    $('#txtFornecedor').val('');
                    $('#txtFormatoBackup').val('');
                    $('#sDestino').val(0);
                });

                //Salvar
                $(this).on("click", "#btnSalvar", function() {
                    $.ajax({
                        url: '/controller/Origem/save.php',
                        method: 'post',
                        data: $('#frmEnviar').serialize(),
                        success: function (data) {
                            var result = JSON.parse(data);
                            if(!result.error){
                                swal({
                                    type: 'success',
                                    title: 'Sucesso!',
                                    text: result.msg
                                });
                                reloadGrid();
                                $('#mdFormOrigem').modal('toggle');
                                $('#txtId').val('');
                                $('#txtNome').val('');
                                $('#txtFornecedor').val('');
                                $('#txtFormatoBackup').val('');
                                $('#sDestino').val(0);
                            } else {
                                swal({
                                    type: 'error',
                                    title: 'Erro!',
                                    text: result.msg
                                });
                            }
                        }
                    });
                });

                //Editar
                $(this).on("click", "#btnEditar", function() {
                    $.ajax({
                        url: '/controller/Origem/getById.php',
                        method: 'get',
                        data: {id: $(this).val()},
                        success: function (data) {
                            var result = JSON.parse(data);
                            $('#txtId').val(result.id);
                            $('#txtNome').val(result.nome);
                            $('#txtFornecedor').val(result.fornecedor);
                            $('#txtFormatoBackup').val(result.formatobackup);
                            $('#sDestino').val(result.iddestino);
                        }
                    });
                });

                //Excluir
                $(this).on("click", "#btnExcluir", function() {
                    swal({
                        title: 'Atenção!',
                        text: "Você deseja mesmo deletar este registro?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Sim'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: '/controller/Origem/delete.php',
                                method: 'delete',
                                data: {id: $(this).val()},
                                success: function (data) {
                                    var result = JSON.parse(data);
                                    if(!result.error){
                                        swal('Sucesso!',
                                            result.msg,
                                            'success'
                                        );
                                        reloadGrid();
                                    } else {
                                        swal('Erro!',
                                            result.msg,
                                            'error'
                                        );
                                    }
                                }
                            });
                        }
                    })
                });

            });

            function reloadGrid() {
                $.ajax({
                    url: '/controller/Origem/getAll.php',
                    method: 'get',
                    success: function (data) {
                        $("#dtOrigem").html(data);
                    }
                });
            }
            </script>
        </body>
    </html>
