<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 13/06/2018
 * Time: 21:01
 */
session_start();
require_once("../model/Origem.php");
$erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>SpMigrações - Apontamento</title>
    <link rel="stylesheet" href="../assets/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/geral.css"/>
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon-32x32.png">
</head>
<body>
<?php include "../header.php"; ?>
<div class="container">
    <form class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" id="frmEnviar" method="post">
        <div class="form-row">
            <div class="form-group col-md-12">
                <h3>Apontamento de Campos</h3>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <select class="form-control" name="sOrigem" id="sOrigem" required>
                    <option value="0">Selecione um Sistema...</option>
                    <?php
                    $oOrigem = new Origem();
                    foreach ($oOrigem->findAll() as $key => $valor) {
                        echo "<option value='" . $valor->id . "'>" . $valor->nome . "</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <div id="resultados"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
            <button id="btnSalvar" type="button" class="btn btn-outline-success">
                <i class="fas fa-save"></i> Salvar
            </button>
            </div>
        </div>
    </form>
</div>
<!--JS'S-->
<script type="text/javascript" src="../assets/jquery-3.3.1.js"></script>
<script type="text/javascript" src="../assets/bootstrap.js"></script>
<script type="text/javascript" src="../assets/sweetalert2.all.min.js"></script>
<!-- Ajax mais loko da vida... -->
<script type="text/javascript">

        var camposMigraveis =[];

        function handlerCampoMigravel(checkbox) {
            if (checkbox.checked) {
                atualizaBadge($('#'+checkbox.id).data("badge"), 'add');
                addCampo(checkbox.value);
            } else {
                atualizaBadge($('#'+checkbox.id).data("badge"), '');
                removeCampo(checkbox.value);
            }
        }
        
        
        function addCampo(value) {
            camposMigraveis.push(value);
        }

        function removeCampo(value) {
            var index = camposMigraveis.indexOf(parseInt(value));
            if (index > -1) {
                camposMigraveis.splice(index, 1);
            }
        }

        function atualizaBadge(badge, type){
            if (type === 'add') {
                $('#'+badge).text(parseInt($('#'+badge).text()) + 1);
            } else {
                $('#'+badge).text(parseInt($('#'+badge).text()) - 1);
            }
        }
    $(document).ready(function () {

        $('#sOrigem').click(function () {
            if ($('#sOrigem').val() > 0) {
                $.ajax({
                    url: '../controller/Apontamento/retornaModulosCampos.php',
                    method: 'post',
                    data: $('#frmEnviar').serialize(),
                    success: function (data) {
                        $('#resultados').html(data);
                        console.log(camposMigraveis);
                    },
                    error: function (data) {
                        $('#resultados').html(data);
                        console.log(camposMigraveis);
                    }
                });
            }
        });

        //Salvar
        $(this).on("click", "#btnSalvar", function() {
            $.ajax({
                url: '/controller/Apontamento/save.php',
                method: 'post',
                data: {listCampos: camposMigraveis, idorigem: $('#sOrigem').val()},
                success: function (data) {
                    var result = JSON.parse(data);
                    if(!result.error){
                        swal({
                            type: 'success',
                            title: 'Sucesso!',
                            text: result.msg
                        });
                        if ($('#sOrigem').val() > 0) {
                            camposMigraveis = [];
                            $.ajax({
                                url: '../controller/Apontamento/retornaModulosCampos.php',
                                method: 'post',
                                data: $('#frmEnviar').serialize(),
                                success: function (data) {
                                    $('#resultados').html(data);
                                },
                                error: function (data) {
                                    $('#resultados').html(data);
                                }
                            });
                        }
                    } else {
                        swal({
                            type: 'error',
                            title: 'Erro!',
                            text: result.msg
                        });
                    }
                }
            });
        });
    });
</script>
<!--FIM DOS JS'S-->
</body>
</html>