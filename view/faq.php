<?php
    session_start();
    require_once("../model/Origem.php");
    $erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>SpMigrações - F.A.Q</title>
    <link rel="stylesheet" href="../assets/bootstrap.css"/>
    <link rel="stylesheet" href="../assets/select2.min.css">
    <link rel="stylesheet" href="../assets/geral.css"/>
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon-32x32.png">
</head>
<body>
<?php include "../header.php"; ?>
<div class="container">
<div class="mb-1 mt-3 col-md-8 offset-md-2">
        <button id="btnNovo" type="button" class="btn btn-primary" data-toggle="modal" data-target="#mdFormFaq">
            <i class="fas fa-plus"></i> Nova Ajuda
        </button>
    </div>
    <div class="modal" id="mdFormFaq">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">F.A.Q</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="frmPesquisa">
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <input type="text" class="form-control" name="txtId" placeholder="ID" id="txtId" readonly/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-10">
                                <input type="text" id="txtBreveDescricao" class="form-control" name="txtBreveDescricao" required placeholder="Descrição Breve"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-10">        
                                <select class="form-control select2" data-placeholder="Selecione o Sistema de Origem" name="sOrigem" style="width: 200px;"
                                        id="sOrigem" required>
                                    <?php
                                    echo "<option value='0'>Selecione a Origem...</option>";
                                    $oOrigem = new Origem();
                                    foreach ($oOrigem->findAll() as $key => $valor) {
                                        echo "<option value='$valor->id'>$valor->nome</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">      
                                <textarea class="form-control" id="txtDescricao" name="txtDescricao" rows="9" cols="80"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="mr-auto">
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="fas fa-times"></i> Sair</button>
                        </div>
                        <div class="ml-auto">
                            <button id="btnSalvar" type="button" class="btn btn-outline-success">
                                <i class="fas fa-save"></i> Salvar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2">
        <div class="form-row">
            <div class="form-group col-md-12">
                <h3>Perguntas Frequentes</h3>
            </div>
            <div class="form-group col-md-12">
                    <h6>Filtros</h3>
            </div>
            <div class="form-row" style="margin-left: 20px;">
                <div class="form-group col-md-12 form-inline">
                    <input class="form-control" type="text" placeholder="Dúvida" id="txtFaqFiltro" name="txtFaqFiltro" style="width: 300px;">
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12" id="dtFaq">
            </div>
        </div>
    </div>
</div>
<!--JS'S-->
<script type="text/javascript" src="../assets/jquery-3.3.1.js"></script>
<script type="text/javascript" src="../assets/bootstrap.js"></script>
<script src="../assets/select2.full.min.js"></script>
<script type="text/javascript" src="../assets/sweetalert2.all.min.js"></script>
<script>
    //select2
    $(".select2").select2();

    $(document).ready(function () {

        reloadGrid();
        
        $("#txtFaqFiltro").keyup(function() {
            if ($("#txtFaqFiltro").val().length > 3) {
                reloadGrid();
            } 
            if ($("#txtFaqFiltro").val() === "") {
                reloadGrid();
            }
        });

        //Novo
        $(this).on("click", "#btnNovo", function() {
            $('#txtId').val('');
            $('#txtBreveDescricao').val('');
            $('#txtDescricao').val('');
            $('#sOrigem').val(0);
        });

        //Salvar
        $(this).on("click", "#btnSalvar", function() {
            $.ajax({
                url: '/controller/Faq/save.php',
                method: 'post',
                data: $('#frmPesquisa').serialize(),
                success: function (data) {
                    var result = JSON.parse(data);
                    if(!result.error){
                        swal({
                            type: 'success',
                            title: 'Sucesso!',
                            text: result.msg
                        });
                        reloadGrid();
                        $('#mdFormFaq').modal('toggle');
                        $('#txtId').val('');
                        $('#txtBreveDescricao').val('');
                        $('#txtDescricao').val('');
                        $('#sOrigem').val(0);
                    } else {
                        swal({
                            type: 'error',
                            title: 'Erro!',
                            text: result.msg
                        });
                    }
                }
            });
        });

        //Editar
        $(this).on("click", "#btnEditar", function() {
            $.ajax({
                url: '/controller/Faq/getById.php',
                method: 'get',
                data: {id: $(this).val()},
                success: function (data) {
                    var result = JSON.parse(data);
                    $('#txtId').val(result.id);
                    $('#txtBreveDescricao').val(result.descricaoBreve);
                    $('#txtDescricao').val(result.descricao);
                    $('#sOrigem').val(result.idorigem);
                }
            });
        });

        //Excluir
        $(this).on("click", "#btnExcluir", function() {
            swal({
                title: 'Atenção!',
                text: "Você deseja mesmo deletar este registro?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sim'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '/controller/Faq/delete.php',
                        method: 'delete',
                        data: {id: $(this).val()},
                        success: function (data) {
                            var result = JSON.parse(data);
                            if(!result.error){
                                swal('Sucesso!',
                                    result.msg,
                                    'success'
                                );
                                reloadGrid();
                            } else {
                                swal('Erro!',
                                    result.msg,
                                    'error'
                                );
                            }
                        }
                    });
                }
            })
        });

    });

    function reloadGrid() {
        $.ajax({
            url: '/controller/Faq/getAll.php',
            method: 'get',
            data: {pergunta: $("#txtFaqFiltro").val()},
            success: function (data) {
                $("#dtFaq").html(data);
            }
        });
    }


</script>
</body>
</html>