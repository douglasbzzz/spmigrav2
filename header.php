
<link rel="stylesheet" href="../assets/fonts/fontawesome-free-5.5.0-web/css/all.css">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="home.php">SpMigrações</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <?php
                    require_once "model/functions.php";
                    if (authWrite($_SESSION['idusuario'])==1){
                       echo "<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\"
                   aria-haspopup=\"true\" aria-expanded=\"false\">
                    Cadastrar
                </a>
                <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                    <a class=\"dropdown-item\" href=\"/view/destinos.php\">Destinos</a>
                    <a class=\"dropdown-item\" href=\"/view/modulos.php\">Módulos Destino</a>
                    <a class=\"dropdown-item\" href=\"/view/campos.php\">Campos Destino</a>
                    <a class=\"dropdown-item\" href=\"/departamentos.php\">Departamentos</a>
                    <div class=\"dropdown-divider\"></div>
                    <a class=\"dropdown-item\" href=\"/view/origens.php\">Origens</a>
                    <a class=\"dropdown-item\" href=\"/view/apontamento.php\">Apontamento</a>
                    <a class=\"dropdown-item\" href=\"/view/faq.php\">F.A.Q</a>
                    <a class=\"dropdown-item\" href=\"/suggest.php\">Sugestões</a>
                </div>";
                    }else{
                        echo "<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\"
                   aria-haspopup=\"true\" aria-expanded=\"false\">
                    Cadastrar
                </a>
                <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                    <a class=\"dropdown-item\" href=\"/suggest.php\">Sugestões</a>
                </div>";
                    }
                ?>

            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <a class="btn btn-outline-secondary" href="/sair.php" role="button"><i class="fas fa-sign-out-alt"></i> Logout</a>
        </ul>
    </div>
</nav>
