<?php
/**
 * Created by PhpStorm.
 * User: darle
 * Date: 15/09/2018
 * Time: 09:17
 */
session_start();
require_once("model/Origem.php");
require_once("model/functions.php");

$idDestino = validaEntrada($_POST['sSistemaFiltro'], 1);
$tipoBackup = validaEntrada($_POST['txtFormatoBackupFiltro'], 0);
$fornecedor = validaEntrada($_POST['txtFornecedorFiltro'], 0);

echo montaComboOrigem($idDestino,$tipoBackup, $fornecedor);

function montaComboOrigem($prIdDestino, $prTipoBackup, $prFornecedor) {
    $str = "";
    $oOrigem = new Origem();
    $str .=  "<option value='0'>Selecione a Origem...</option>";
    foreach ($oOrigem->retOrigemFiltro($prIdDestino, $prTipoBackup, $prFornecedor) as $key => $valor){
        $str .= "<option value='".$valor->id."'>".$valor->nomeorigem."</option>";
    }
    return $str;
}

//region Validações

function validaEntrada($prValor, $prTipo)
{
    if ($prTipo == 0) {
        if (isset($prValor) && $prValor <> "") {
            $valorReturn = $prValor;
        } else {
            $valorReturn = "";
        }
    } else if ($prTipo == 1) {
        if (isset($prValor) && $prValor > 0) {
            $valorReturn = $prValor;
        } else {
            $valorReturn = 0;
        }
    } else {
        $valorReturn = null;
    }
    return $valorReturn;
}

//endregion