<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 26/07/2018
 * Time: 22:53
 */
    session_start();
    $erro = isset($_GET['erro']) ? $_GET['erro'] : 3;

?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8" />
        <title>SpMigrações - Dptos.</title>
        <link rel="stylesheet" href="assets/bootstrap.css" />
        <link rel="stylesheet" href="assets/select2.min.css">
        <link rel="stylesheet" href="assets/geral.css" />
        <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32x32.png">
    </head>

    <body>
        <?php include "header.php"; ?>
        <div class="container">
            <form class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" id="frmPesquisa" action="departamentoController.php" method="post">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Cadastro de Departamentos Internos</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <input type="text" id="txtNome" class="form-control" name="txtNome" required placeholder="Nome Dpto." />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <input type="text" id="txtNome" class="form-control" name="txtGestor" required placeholder="Nome Gestor" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <input type="text" id="txtNome" class="form-control" name="txtCoor" required placeholder="Nome Coordenador" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <input type="submit" name="btnEnvia" class="btn btn-outline-primary" id="btnEnvia" value="Salvar" />

                        <?php
                if ($erro == 1) {
                    echo '<font color="#FF0000">Problema para salvar o departamento, Verifique!<br/></font>';
                } elseif ($erro == 2) {
                    echo '<font color="#008000">Departamento salvo!!<br/></font>';
                }
                ?>

                    </div>
                </div>
            </form>
            <div id="migr" class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" hidden>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Campos Migrados</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div id="result">
                    </div>
                </div>
            </div>
        </div>
        <!--JS'S-->
        <script type="text/javascript" src="assets/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="assets/bootstrap.js"></script>
        <script src="assets/select2.full.min.js"></script>
        <script>
            //select2
            $(".select2").select2();

        </script>
    </body>

    </html>
