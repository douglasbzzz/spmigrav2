<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 03/09/2018
 * Time: 21:15
 */

session_start();
$erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>SpMigrações - Sugestões</title>
    <link rel="stylesheet" href="assets/bootstrap.css"/>
    <link rel="stylesheet" href="assets/select2.min.css">
    <link rel="stylesheet" href="assets/geral.css"/>
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32x32.png">
</head>
<body>
<?php include "header.php"; ?>

<form class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" id="frmPesquisa"
      action="suggestController.php" method="post">
    <div class="form-row">
        <div class="form-group col-md-10">
            <h3>Formulário de Sugestões</h3>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <textarea class="form-control" id="txtDescricao" name="txtDescricao" rows="9" cols="80"></textarea>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <select class="form-control" name="sPrioridade" id="sPrioridade">
                <option value="0">Selecione uma Prioridade</option>
                <option value="1">Baixa</option>
                <option value="2">Normal</option>
                <option value="3">Urgente</option>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <input type="submit" name="btnEnvia" class="btn btn-outline-primary" id="btnEnvia" value="Criar Registro"/>
        </div>
    </div>
    <?php
    if ($erro == 1) {
        echo '<font color="#FF0000">Problema ao incluir registro. Verifique!<br/></font>';
    } elseif ($erro == 2) {
        echo '<font color="#008000">Registro Criado!<br/></font>';
    }
    ?>
    <br/>
</form>
<!--JS'S-->
<script type="text/javascript" src="assets/jquery-3.3.1.js"></script>
<script type="text/javascript" src="assets/bootstrap.js"></script>
<script src="assets/select2.full.min.js"></script>
<script>
    //select2
    $(".select2").select2();
</script>
</body>
</html>
