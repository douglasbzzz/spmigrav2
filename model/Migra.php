<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 03/07/2018
 * Time: 00:04
 */
require_once("Crud.php");

class Migra extends Crud
{
    protected $table = "migra";

    private $idCampo;
    private $idOrigem;

    public function insert()
    {
        $sql = "insert into $this->table (idorigem, idcampo) values (:origem, :campo)";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':origem', $this->idOrigem);
        $stmt->bindParam(':campo', $this->idCampo);
        return $stmt->execute();
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    /**
     * @return mixed
     */
    public function getIdCampo()
    {
        return $this->idCampo;
    }

    /**
     * @param mixed $idCampo
     */
    public function setIdCampo($idCampo): void
    {
        $this->idCampo = $idCampo;
    }

    /**
     * @return mixed
     */
    public function getIdOrigem()
    {
        return $this->idOrigem;
    }

    /**
     * @param mixed $idOrigem
     */
    public function setIdOrigem($idOrigem): void
    {
        $this->idOrigem = $idOrigem;
    }

    public function retMigraByModulo($idModulo, $idOrigem)
    {
        if ($idModulo > 0) {
            $sql = "select o.id as idorigem, c.id as idcampo, c.nomecampodb as nomeCampo, c.aliascampo as campoFull  
                    from migra m
                    inner join origem o on o.id = m.idorigem
                    inner join campos c on c.id = m.idcampo
                    inner join modulos mo on mo.id = c.idmodulo
                    where c.idmodulo = :modulo and o.id = :origem";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':modulo', $idModulo);
            $stmt->bindParam(':origem', $idOrigem);
            $stmt->execute();
            return $stmt->fetchAll();
        } else {
            echo "Não foi possível carregar os Campos à partir do Modulo informado! Verifique.";
        }
    }

    public function deleteByOrigem($id){
        $sql = "DELETE FROM $this->table WHERE idorigem = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        return $stmt->execute();
    }

}

?>