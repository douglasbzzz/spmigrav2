<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 22/06/2018
 * Time: 22:13
 */
require_once("Crud.php");
class Modulo extends Crud{
    protected $table = "modulos";

    private $nome;
    private $destino;

    public function insert(){
        $sql = "insert into $this->table (nome, iddestino) values (:nome,:destino)";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':nome',$this->nome);
        $stmt -> bindParam(':destino',$this->destino);
        return $stmt ->execute();
    }

    public function update($id){
        $sql = "UPDATE $this->table set nome = :nome, iddestino = :destino WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':nome',$this->nome);
        $stmt -> bindParam(':destino',$this->destino);
        $stmt -> bindParam(':id',$id);
        return $stmt ->execute();
    }

    public function retModulos(){
        $sql = "select distinct m.id as moduloid, m.nome as nomemodulo, d.nome as nomedestino
                from modulos as m 
                inner join destino as d on d.id = m.iddestino
                order by moduloid, nomemodulo, nomedestino";
        $stmt = DB::prepare($sql);
        $stmt ->execute();
        return $stmt->fetchAll();
    }

    public function retModulosByDestino($idDestino){
        if($idDestino>0){
            $sql ="select distinct m.id as moduloid, m.nome as nomemodulo, d.nome as nomedestino
                from modulos as m 
                inner join destino as d on d.id = m.iddestino
                where m.iddestino = :destino";
            $stmt = DB::prepare($sql);
            $stmt ->bindParam(':destino',$idDestino);
            $stmt->execute();
            return $stmt->fetchAll();
        }else {
            echo "Não foi possível carregar os Módulos à partir do destino informado! Verifique.";
        }
    }

    public function retModulosByOrigem($idOrigem){
        if($idOrigem>0){
            $sql = "select m.id as idmodulo, m.nome as nomemodulo, m.iddestino
                    from modulos as m
                    inner join destino as d on d.id = m.iddestino
                    inner join origem as o on o.iddestino = d.id
                    where o.id = :origem";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':origem',$idOrigem);
            $stmt ->execute();
            return $stmt->fetchAll();
        }else{
            echo "Não foi possível carregar os Módulos à partir da origem informada! Verifique.";
        }
    }


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * @param mixed $destino
     */
    public function setDestino($destino): void
    {
        $this->destino = $destino;
    }

    /**
     * @return mixed
     */

    public function findByID($id){
        $sql = "SELECT Modulos.Id as ModuloID, Modulos.Nome as ModuloNome, Destino.id as DestinoID FROM $this->table INNER JOIN Destino ON Destino.id = Modulos.iddestino WHERE Modulos.id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

}

?>