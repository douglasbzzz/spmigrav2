<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 22/06/2018
 * Time: 22:00
 */

require_once("Crud.php");

class Destino extends Crud{
    protected $table = "destino";

    private $nome;

    public function insert(){
        $sql = "insert into $this->table (nome) values (:nome)";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':nome',$this->nome);
        return $stmt ->execute();
    }

    public function update($id){
        $sql = "UPDATE $this->table set nome = :nome WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':nome',$this->nome);
        $stmt -> bindParam(':id',$id);
        return $stmt ->execute();
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }
}
?>