<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 03/09/2018
 * Time: 21:49
 */
    require_once("Crud.php");
    class Sugestao extends Crud {
        protected $table = "sugestao";
        private $descricao;
        private $idUsuario;
        private $dataCadastro;
        private $dataResolucao;
        private $situacao;
        //private $codigo;
        private $prioridade;

        /**
         * @return mixed
         */
        public function getDescricao()
        {
            return $this->descricao;
        }

        /**
         * @return mixed
         */
        public function getDataResolucao()
        {
            return $this->dataResolucao;
        }

        /**
         * @param mixed $dataResolucao
         */
        public function setDataResolucao($dataResolucao)
        {
            $this->dataResolucao = $dataResolucao;
        }



        /**
         * @param mixed $descricao
         */
        public function setDescricao($descricao)
        {
            $this->descricao = $descricao;
        }

        /**
         * @return mixed
         */
        public function getIdUsuario()
        {
            return $this->idUsuario;
        }

        /**
         * @param mixed $idUsuario
         */
        public function setIdUsuario($idUsuario)
        {
            $this->idUsuario = $idUsuario;
        }

        /**
         * @return mixed
         */
        public function getDataCadastro()
        {
            return $this->dataCadastro;
        }

        /**
         * @param mixed $data
         */
        public function setDataCadastro($dataCadastro)
        {
            $this->dataCadastro = $dataCadastro;
        }

        /**
         * @return mixed
         */
        public function getSituacao()
        {
            return $this->situacao;
        }

        /**
         * @param mixed $situacao
         */
        public function setSituacao($situacao)
        {
            $this->situacao = $situacao;
        }

        /**
         * @return mixed
         */
        public function getPrioridade()
        {
            return $this->prioridade;
        }

        /**
         * @param mixed $prioridade
         */
        public function setPrioridade($prioridade)
        {
            $this->prioridade = $prioridade;
        }

        public function insert(){
            $sql = "insert into $this->table(descricao, idUsuario, situacao, prioridade) 
                    values (:describe,:idUser,:situation,:priority)";
            $smtp=DB::prepare($sql);
            $smtp ->bindParam(':describe', $this->descricao);
            $smtp ->bindParam(':idUser', $this->idUsuario);
            $smtp ->bindParam(':situation',$this->situacao);
            $smtp ->bindParam(':priority',$this->prioridade);
            return $smtp->execute();
        }

        public function update($id)
        {
            // TODO: Implement update() method.
        }

    }

?>