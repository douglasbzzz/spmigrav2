<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 22/06/2018
 * Time: 21:55
 */
require_once("Crud.php");

class Origem extends Crud
{
    protected $table = "origem";

    private $nome;
    private $fornecedor;
    private $formatobackup;
    private $destino;

    public function insert()
    {
        $sql = "insert into $this->table (nome, fornecedor, iddestino, formatobackup) values (:nome,:fornecedor, :destino ,:formatobackup)";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':nome', $this->nome);
        $stmt->bindParam(':fornecedor', $this->fornecedor);
        $stmt->bindParam(':destino', $this->destino);
        $stmt->bindParam(':formatobackup', $this->formatobackup);

        return $stmt->execute();

    }

    public function update($id){
        $sql = "UPDATE $this->table set nome = :nome, fornecedor = :fornecedor, formatobackup = :formatobackup, iddestino = :destino WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':nome',$this->nome);
        $stmt -> bindParam(':fornecedor',$this->fornecedor);
        $stmt -> bindParam(':formatobackup',$this->formatobackup);
        $stmt -> bindParam(':destino',$this->destino);
        $stmt -> bindParam(':id',$id);
        return $stmt ->execute();
    }

    public function retOrigem()
    {
        $sql = "select distinct o.id, o.nome as nomeorigem, o.fornecedor as nomefornecedor, o.formatobackup, d.nome as nomedestino
                    from origem as o 
                    inner join destino as d on d.id = o.iddestino
                    order by nomeorigem";
        $stmt = DB::prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function retOrigemInformacoesGerais($prIdOrigem)
    {
        $sql = "select o.id, o.nome as nomeorigem, o.fornecedor as nomefornecedor, o.formatobackup, d.nome as nomedestino
                    from origem as o 
                    inner join destino as d on d.id = o.iddestino
                    where o.id = :origem 
                    LIMIT 1";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':origem', $prIdOrigem);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function retOrigemFiltro($idDestino, $tipoBackup, $fornecedor)
    {
        $sql = "select distinct o.id, o.nome as nomeorigem, o.fornecedor as nomefornecedor, o.formatobackup from origem as o 
                inner join destino as d on d.id = o.iddestino where 1=1";
        if ($idDestino <> 0) {
            echo $idDestino;
            $sql .= " and d.id = :destino ";
        }
        if ($fornecedor <> '') {
            echo $fornecedor;
            $sql .= " and o.fornecedor like :fornecedor ";
        }
        if ($tipoBackup <> '') {
            echo $tipoBackup;
            $sql .= " and o.formatobackup like :backup ";
        }
        $stmt = DB::prepare($sql);
        if ($idDestino <> 0) {
            $stmt->bindParam(':destino', $idDestino);
        }
        if ($fornecedor <> '') {
            $fornecedor = '%'.$fornecedor. '%';
            $stmt->bindParam(':fornecedor',$fornecedor );
        }
        if ($tipoBackup <> '') {
            $tipoBackup = '%'.$tipoBackup. '%';
            $stmt->bindParam(':backup', $tipoBackup);
        }
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    /**
     * @param mixed $fornecedor
     */
    public function setFornecedor($fornecedor)
    {
        $this->fornecedor = $fornecedor;
    }

    /**
     * @return mixed
     */
    public function getFormatobackup()
    {
        return $this->formatobackup;
    }

    /**
     * @param mixed $formatobackup
     */
    public function setFormatobackup($formatobackup)
    {
        $this->formatobackup = $formatobackup;
    }

    /**
     * @return mixed
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * @param mixed $destino
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;
    }


}

?>
