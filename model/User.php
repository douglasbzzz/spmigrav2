<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 26/07/2018
 * Time: 23:13
 */
    require_once("Crud.php");

    class User extends Crud{
        protected $table="usuario";

        private $nome;
        private $login;
        private $senha;
        private $email;
        private $idDpto;
        private $ativo;

        /**
         * @return mixed
         */
        public function getNome()
        {
            return $this->nome;
        }

        /**
         * @param mixed $nome
         */
        public function setNome($nome)
        {
            $this->nome = $nome;
        }

        /**
         * @return mixed
         */
        public function getLogin()
        {
            return $this->login;
        }

        /**
         * @param mixed $login
         */
        public function setLogin($login)
        {
            $this->login = $login;
        }

        /**
         * @return mixed
         */
        public function getSenha()
        {
            return $this->senha;
        }

        /**
         * @param mixed $senha
         */
        public function setSenha($senha)
        {
            $this->senha = $senha;
        }

        /**
         * @return mixed
         */
        public function getEmail()
        {
            return $this->email;
        }

        /**
         * @param mixed $email
         */
        public function setEmail($email)
        {
            $this->email = $email;
        }

        /**
         * @return mixed
         */
        public function getIdDpto()
        {
            return $this->idDpto;
        }

        /**
         * @param mixed $idDpto
         */
        public function setIdDpto($idDpto)
        {
            $this->idDpto = $idDpto;
        }

        /**
         * @return mixed
         */
        public function getAtivo()
        {
            return $this->ativo;
        }

        /**
         * @param mixed $ativo
         */
        public function setAtivo($ativo)
        {
            $this->ativo = $ativo;
        }

        public function insert(){
            $sql = "insert into $this->table (nome, login, senha, email, iddepartamento) values (:nomeUser, :loginUser, :pass, :mail, :idDpto)";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':nomeUser',$this->nome);
            $stmt->bindParam(':loginUser',$this->login);
            $stmt->bindParam(':pass',$this->senha);
            $stmt->bindParam(':mail',$this->email);
            $stmt->bindParam(':idDpto',$this->idDpto);
            return $stmt->execute();
        }

        public function update($id)
        {
            // TODO: Implement update() method.
        }

        public function auth($user, $pass){
            if($user <> "" && $pass <> ""){
                $sql="select u.id as idUser, u.nome, u.login, u.email, u.ativo as situacao, d.nome as departamento
                  from usuario as u
                  inner join departamento as d on d.id = u.iddepartamento
                  where ((login = :usuario and senha = :senha) and u.ativo = 1)";
                $stmt = DB::prepare($sql);
                $stmt ->bindParam(':usuario',$user);
                $stmt ->bindParam(':senha',$pass);
                $stmt->execute();
                //return $stmt->fetchAll();
            }else{
                echo "Houve um problema para recuperar as informações do usuário... verifique!";
            }


        }


    }

?>