<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 22/06/2018
 * Time: 22:18
 */

require_once("Crud.php");
class Campos extends Crud{
    protected $table = "campos";

    private $modulo;
    private $nomeCampo;
    private $apelidoCampo;

    public function insert(){
        $sql = "insert into $this->table (idmodulo, nomecampodb, aliascampo) values (:idmodulo,:nomecampo,:apelidocampo)";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':idmodulo',$this->modulo);
        $stmt -> bindParam(':nomecampo',$this->nomeCampo);
        $stmt -> bindParam(':apelidocampo',$this->apelidoCampo);
        return $stmt ->execute();
    }

    public function update($id)
    {
        $sql = "UPDATE $this->table set nomecampodb = :nome, aliascampo = :apelido, idmodulo = :idmodulo WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt -> bindParam(':nome',$this->nomeCampo);
        $stmt -> bindParam(':apelido',$this->apelidoCampo);
        $stmt -> bindParam(':idmodulo',$this->modulo);
        $stmt -> bindParam(':id',$id);
        return $stmt ->execute();
    }

    /**
     * @return mixed
     */
    public function getModulo()
    {
        return $this->modulo;
    }

    /**
     * @param mixed $modulo
     */
    public function setModulo($modulo): void
    {
        $this->modulo = $modulo;
    }

    /**
     * @return mixed
     */
    public function getNomeCampo()
    {
        return $this->nomeCampo;
    }

    /**
     * @param mixed $nomeCampo
     */
    public function setNomeCampo($nomeCampo): void
    {
        $this->nomeCampo = $nomeCampo;
    }

    /**
     * @return mixed
     */
    public function getApelidoCampo()
    {
        return $this->apelidoCampo;
    }

    /**
     * @param mixed $apelidoCampo
     */
    public function setApelidoCampo($apelidoCampo): void
    {
        $this->apelidoCampo = $apelidoCampo;
    }

    public function retCamposByModulo($idModulo){
        $sql = "select c.id as idcampo, c.idmodulo, c.aliascampo, m.nome as nomemodulo,
                d.nome as nomedestino
                from campos as c 
                inner join modulos as m on m.id = c.idmodulo
                inner join destino as d on d.id = m.iddestino
                where c.idmodulo = :modulo";
        $stmt = DB::prepare($sql);
        $stmt ->bindParam(':modulo',$idModulo);
        $stmt ->execute();
        return $stmt->fetchAll();

    }

    public function retCampos($prIdDestino, $prIdModulo){
        $sql = "select c.id as id, c.idmodulo, c.nomecampodb as nomecampo, c.aliascampo as apelido, m.nome as nomemodulo,
                d.nome as nomedestino
                from campos as c 
                inner join modulos as m on m.id = c.idmodulo 
                inner join destino as d on d.id = m.iddestino
                where 1 = 1";
                
        if ($prIdDestino > 0) {
            $sql .= " and m.iddestino = :destino";
        }
        
        if ($prIdModulo > 0) {
            $sql .= " and c.idmodulo = :modulo";
        }

        $stmt = DB::prepare($sql);
        if ($prIdDestino > 0) {
            $stmt ->bindParam(':destino',$prIdDestino);
        }
        
        if ($prIdModulo > 0) {
            $stmt ->bindParam(':modulo',$prIdModulo);
        }
        $stmt ->execute();
        return $stmt->fetchAll();

    }

    public function findByID($id){
        $sql = "select c.id as id, c.idmodulo, c.nomecampodb as nomecampo, c.aliascampo as apelido, m.id as idmodulo,
                d.id as iddestino
                from campos as c 
                inner join modulos as m on m.id = c.idmodulo
                inner join destino as d on d.id = m.iddestino
                where c.id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':id',$id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch();
    }

}
?>