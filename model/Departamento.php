<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 26/07/2018
 * Time: 22:38
 */
    require_once("Crud.php");
    class Departamento extends Crud{
        protected $table = "departamento";

        private $nome;
        private $nomeGestor;
        private $nomeCoordenador;

        /**
         * @return mixed
         */
        public function getNome()
        {
            return $this->nome;
        }

        /**
         * @param mixed $nome
         */
        public function setNome($nome)
        {
            $this->nome = $nome;
        }

        /**
         * @return mixed
         */
        public function getNomeGestor()
        {
            return $this->nomeGestor;
        }

        /**
         * @param mixed $nomeGestor
         */
        public function setNomeGestor($nomeGestor)
        {
            $this->nomeGestor = $nomeGestor;
        }

        /**
         * @return mixed
         */
        public function getNomeCoordenador()
        {
            return $this->nomeCoordenador;
        }

        /**
         * @param mixed $nomeCoordenador
         */
        public function setNomeCoordenador($nomeCoordenador)
        {
            $this->nomeCoordenador = $nomeCoordenador;
        }

        public function insert(){
            $sql = "insert into $this->table (nome, nomeGestor, nomeCoordenador) value (:nomeDpto, :nomeGes, :nomeCoo)";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':nomeDpto',$this->nome);
            $stmt->bindParam(':nomeGes',$this->nomeGestor);
            $stmt->bindParam(':nomeCoo',$this->nomeCoordenador);
            return $stmt->execute();
        }

        public function update($id)
        {
            // TODO: Implement update() method.
        }


    }

?>