<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 05/08/2018
 * Time: 15:51
 */

require_once("Crud.php");

class Faq extends Crud {
    protected $table = "faq";
    private $shortDesc;
    private $idUsuario;
    private $dataInclusao;
    private $lastEdit;
    private $idUserLastEdicao;
    private $origem;
    private $descricao;

    /**
     * @return mixed
     */
    public function getShortDesc()
    {
        return $this->shortDesc;
    }

    /**
     * @param mixed $longDesc
     */
    public function setShortDesc($shortDesc)
    {
        $this->shortDesc = $shortDesc;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getDataInclusao()
    {
        return $this->dataInclusao;
    }

    /**
     * @param mixed $dataInclusao
     */
    public function setDataInclusao($dataInclusao)
    {
        $this->dataInclusao = $dataInclusao;
    }

    /**
     * @return mixed
     */
    public function getLastEdit()
    {
        return $this->lastEdit;
    }

    /**
     * @param mixed $lastEdit
     */
    public function setLastEdit($lastEdit)
    {
        $this->lastEdit = $lastEdit;
    }

    /**
     * @return mixed
     */
    public function getIdUserLastEdicao()
    {
        return $this->idUserLastEdicao;
    }

    /**
     * @param mixed $idUserLastEdicao
     */
    public function setIdUserLastEdicao($idUserLastEdicao)
    {
        $this->idUserLastEdicao = $idUserLastEdicao;
    }

    /**
     * @return mixed
     */
    public function getOrigem()
    {
        return $this->origem;
    }

    /**
     * @param mixed $origem
     */
    public function setOrigem($origem)
    {
        $this->origem = $origem;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    public function insert(){
        $sql = "insert into $this->table (descricaobreve, idusuario, idusuariolastedicao, idorigem, descricao) 
                            values (:descBreve,:idUser,:idUserLastEdit,:idOrigem,:conteudo)";
        $smtp=DB::prepare($sql);
        $smtp ->bindParam(':descBreve', $this->shortDesc);
        $smtp ->bindParam(':idUser', $this->idUsuario);
        $smtp ->bindParam(':idUserLastEdit',$this->idUserLastEdicao);
        $smtp ->bindParam(':idOrigem',$this->origem);
        $smtp ->bindParam(':conteudo',$this->descricao);
        return $smtp->execute();
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function retFaq($pergunta){
        $sql = "select faq.id, faq.descricaobreve, faq.descricao, usuario.nome as usuario, origem.nome as nomeorigem from faq 
        inner join usuario on usuario.id = faq.idusuario 
        inner join origem on origem.id = faq.idorigem";
        if ($pergunta != "") {
            $sql .= " where faq.descricaobreve like :pergunta or faq.descricao like :pergunta or usuario.nome like :pergunta";
        }
        $stmt = DB::prepare($sql);
        if ($pergunta != "") {
            $pergunta = "%" . $pergunta . "%";
            $stmt ->bindParam(':pergunta', $pergunta);
        }
        $stmt ->execute();
        return $stmt->fetchAll();
    }


}