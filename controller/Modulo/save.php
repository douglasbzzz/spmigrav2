<?php
require_once("../../model/Modulo.php");

$id = $_POST['txtId'];
$nome = $_POST['txtNome'];
$idDestino = $_POST['sDestino'];

if ($nome == null || $nome == "") {
    $results = array(
        'error' => true,
        'msg' => 'Nome inválido!'
    );
    echo json_encode($results);
    exit();
}

if($idDestino == 0){
    $results = array(
        'error' => true,
        'msg' => 'Destino selecionado inválido!'
    );
    echo json_encode($results);
    exit();
}

$oModulo = new Modulo();
$oModulo->setNome($nome);
$oModulo->setDestino($idDestino);

try{
    if ($id > 0 && $id <> "") {
        if ($oModulo->update($id)) {
            $results = array(
                'error' => false,
                'msg' => 'Módulo alterado com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao alterar o Módulo! Verifique as informações!'
            );
        }
    } else {
        if ($oModulo->insert()) {
            $results = array(
                'error' => false,
                'msg' => 'Módulo salvo com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao salvar o Módulo! Verifique as informações!'
            );
        }
    }
    echo json_encode($results);
}catch (Exception $e){
    $results = array(
        'error' => true,
        'msg' => 'Erro ao salvar o Módulo!' . $e->getMessage()
    );
    echo json_encode($results);
}