<?php

session_start();
require_once("../../model/Modulo.php");

echo getDatatable();

function getDatatable() {
    $oModulo = new Modulo();
    $str = "<table class='table table-hover'>";
    $str .= "<thead class='thead-light'>";
    $str .= "<tr>";
    $str .= "<th>Módulo</th>";
    $str .= "<th>Sistema</th>";
    $str .= "<th  style='width:  20%'>Ações</th>";
    $str .= "</tr>";
    $str .= "</thead>";
    foreach ($oModulo->retModulos() as $key => $valor){
        $str .= "<tbody>";
        $str .= "<tr>";
        $str .= "<td>$valor->nomemodulo</td>";
        $str .= "<td>$valor->nomedestino</td>";
        $str .= "<td>";
        $str .= "<button id='btnEditar' type='button' class='btn btn-outline-secondary mr-2' value='$valor->moduloid' data-toggle='modal' data-target='#mdFormModulo'>";
        $str .= "<i class='fas fa-edit'></i>";
        $str .= "</button>";
        $str .= "<button id='btnExcluir' type='button' class='btn btn-outline-danger' value='$valor->moduloid'>";
        $str .= "<i class='fas fa-trash-alt'></i>";
        $str .= "</button>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "</tbody>";
    }
    $str .= "</table>";
    return $str;
}