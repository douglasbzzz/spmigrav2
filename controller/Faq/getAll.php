<?php

session_start();
require_once("../../model/Faq.php");

$pergunta = $_GET['pergunta'];

echo getDatatable($pergunta);

function getDatatable($pergunta) {
    $oFaq = new Faq();
    $str = "<table class='table table-hover'>";
    $str .= "<thead class='thead-light'>";
    $str .= "<tr>";
    $str .= "<th>Breve descrição</th>";
    $str .= "<th>Descrição</th>";
    $str .= "<th>Origem</th>";
    $str .= "<th>Usuário</th>";
    $str .= "</thead>";
    foreach ($oFaq->retFaq($pergunta) as $key => $valor){
        $str .= "<tbody>";
        $str .= "<tr>";
        $str .= "<td>$valor->descricaobreve</td>";
        $str .= "<td>$valor->descricao</td>";
        $str .= "<td>$valor->nomeorigem</td>";
        $str .= "<td>$valor->usuario</td>";
        $str .= "</tr>";
        $str .= "</tbody>";
    }
    $str .= "</table>";
    return $str;
}