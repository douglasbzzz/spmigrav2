<?php
session_start();
require_once("../../model/Faq.php");

$id = $_POST['txtId'];
$descricao = $_POST['txtDescricao'];
$breveDescricao = $_POST['txtBreveDescricao'];
$idOrigem = $_POST['sOrigem'];

if ($descricao == null || $descricao == "") {
    $results = array(
        'error' => true,
        'msg' => 'Descricão inválida!'
    );
    echo json_encode($results);
    exit();
}

if ($breveDescricao == null || $breveDescricao == "") {
    $results = array(
        'error' => true,
        'msg' => 'Descricão Breve inválida!'
    );
    echo json_encode($results);
    exit();
}

if($idOrigem == 0){
    $results = array(
        'error' => true,
        'msg' => 'Origem selecionada inválida!'
    );
    echo json_encode($results);
    exit();
}

$oFaq = new Faq();
$oFaq->setDescricao($descricao);
$oFaq->setShortDesc($breveDescricao);
$oFaq->setOrigem($idOrigem);
$oFaq ->setIdUsuario($_SESSION['idusuario']);
$oFaq ->setIdUserLastEdicao($_SESSION['idusuario']);

try{
    if ($id > 0 && $id <> "") {
        if ($oFaq->update($id)) {
            $results = array(
                'error' => false,
                'msg' => 'FAQ alterado com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao alterar o FAQ! Verifique as informações!'
            );
        }
    } else {
        if ($oFaq->insert()) {
            $results = array(
                'error' => false,
                'msg' => 'FAQ salvo com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao salvar o FAQ! Verifique as informações!'
            );
        }
    }
    echo json_encode($results);
}catch (Exception $e){
    $results = array(
        'error' => true,
        'msg' => 'Erro ao salvar o FAQ!' . $e->getMessage()
    );
    echo json_encode($results);
}