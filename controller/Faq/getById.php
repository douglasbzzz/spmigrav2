<?php
    session_start();
    require_once("../../model/Modulo.php");

    $id = $_GET['id'];

    if ($id == null || $id == 0) {
        $results = array(
            'error' => true,
            'msg' => 'Registro inválido!'
        );
        echo json_encode($results);
        exit();
    }

    $modulo = new Modulo();
    $moduloEdit = $modulo->findByID($id);

    echo json_encode($moduloEdit);
