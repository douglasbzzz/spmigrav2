<?php

session_start();
require_once("../../model/Origem.php");

echo getDatatable();

function getDatatable() {
    $oOrigem = new Origem();
    $str = "<table class='table table-hover'>";
    $str .= "<thead class='thead-light'>";
    $str .= "<tr>";
    $str .= "<th>Origem</th>";
    $str .= "<th>Fornecedor</th>";
    $str .= "<th>Backup</th>";
    $str .= "<th>Destino</th>";
    $str .= "<th  style='width:  20%'>Ações</th>";
    $str .= "</tr>";
    $str .= "</thead>";
    foreach ($oOrigem->retOrigem() as $key => $valor){
        $str .= "<tbody>";
        $str .= "<tr>";
        $str .= "<td>$valor->nomeorigem</td>";
        $str .= "<td>$valor->nomefornecedor</td>";
        $str .= "<td>$valor->formatobackup</td>";
        $str .= "<td>$valor->nomedestino</td>";
        $str .= "<td>";
        $str .= "<button id='btnEditar' type='button' class='btn btn-outline-secondary mr-2' value='$valor->id' data-toggle='modal' data-target='#mdFormOrigem'>";
        $str .= "<i class='fas fa-edit'></i>";
        $str .= "</button>";
        $str .= "<button id='btnExcluir' type='button' class='btn btn-outline-danger' value='$valor->id'>";
        $str .= "<i class='fas fa-trash-alt'></i>";
        $str .= "</button>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "</tbody>";
    }
    $str .= "</table>";
    return $str;
}