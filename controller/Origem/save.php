<?php
require_once("../../model/Origem.php");

$id = $_POST['txtId'];
$nome = $_POST['txtNome'];
$fornecedor = $_POST['txtFornecedor'];
$formatoBackup = $_POST['txtFormatoBackup'];
$idDestino = $_POST['sDestino'];

if ($nome == null || $nome == "") {
    $results = array(
        'error' => true,
        'msg' => 'Nome inválido!'
    );
    echo json_encode($results);
    exit();
}

if ($fornecedor == null || $fornecedor == "") {
    $results = array(
        'error' => true,
        'msg' => 'Fornecedor inválido!'
    );
    echo json_encode($results);
    exit();
}

if ($formatoBackup == null || $formatoBackup == "") {
    $results = array(
        'error' => true,
        'msg' => 'Formato do backup inválido!'
    );
    echo json_encode($results);
    exit();
}

if($idDestino == 0){
    $results = array(
        'error' => true,
        'msg' => 'Destino selecionado inválido!'
    );
    echo json_encode($results);
    exit();
}

$oOrigem = new Origem();
$oOrigem->setNome($nome);
$oOrigem->setFormatobackup($formatoBackup);
$oOrigem->setFornecedor($fornecedor);
$oOrigem->setDestino($idDestino);

try{
    if ($id > 0 && $id <> "") {
        if ($oOrigem->update($id)) {
            $results = array(
                'error' => false,
                'msg' => 'Origem alterada com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao alterar a Origem! Verifique as informações!'
            );
        }
    } else {
        if ($oOrigem->insert()) {
            $results = array(
                'error' => false,
                'msg' => 'Origem salva com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao salvar a Origem! Verifique as informações!'
            );
        }
    }
    echo json_encode($results);
}catch (Exception $e){
    $results = array(
        'error' => true,
        'msg' => 'Erro ao salvar a Origem!' . $e->getMessage()
    );
    echo json_encode($results);
}