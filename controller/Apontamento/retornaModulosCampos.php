<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 26/06/2018
 * Time: 21:20
 */

    session_start();
    require_once("../../model/Modulo.php");
    require_once("../../model/Campos.php");
    require_once("../../model/Migra.php");

    //echo "A origem é..: ".$_POST['sOrigem'];

    if(isset($_POST['sOrigem']) && $_POST['sOrigem'] >0){
        $idOrigem = $_POST['sOrigem'];
    }else{
        $idOrigem = 0;
    }
    //$idOrigem = 21;

    $oModulo = new Modulo();

    echo "<a type='hidden' name='idOrigem' id='idOrigem' value='".$idOrigem."'></a>";

    foreach($oModulo->retModulosByOrigem($idOrigem) as $key => $value){
        echo "<div class='card'>";
            echo "<div class='card-header'>";
                echo "<h4>";
                    echo "<spam data-toggle='collapse' href='#collapse$key'>$value->nomemodulo</spam>";
                    echo "<span id='badge$key' class='float-right badge badge-secondary'>";
                        $oMigra= new Migra();
                        echo count($oMigra->retMigraByModulo($value->idmodulo, $idOrigem));
                    echo "</span>";
                echo "</h4>";
            echo "</div>";
            echo "<div id='collapse$key' class='panel-collapse collapse'>";
                echo "<ul class='list-inline'>";
                    $oCamposModulo = new Campos();
                    foreach($oCamposModulo->retCamposByModulo($value->idmodulo) as $chave => $valor){
                        echo "<li class='list-inline-item'>";
                        $check = "";
                        foreach($oMigra->retMigraByModulo($value->idmodulo,$idOrigem) as $k => $v){
                            if ($v->idcampo == $valor->idcampo) {
                                $check = "checked";
                                echo "<script>camposMigraveis.push($valor->idcampo);</script>";
                            }
                        }
                        echo "<label><input data-badge='badge$key' type='checkbox' id='cbCampoID$valor->idcampo' onchange='handlerCampoMigravel(this);' name='campo[]' value='$valor->idcampo' $check/>$valor->aliascampo</label>";
                        echo "</li>";
                    }
                echo "</ul>";
            echo "</div>";
        echo "</div>";
    }
?>