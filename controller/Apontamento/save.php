<?php

    require_once("../../model/Migra.php");

    $idOrigem = $_POST['idorigem'];
    $listCampos = $_POST['listCampos'];

    if ($idOrigem == 0) {
        $results = array(
            'error' => true,
            'msg' => 'Origem inválida!'
        );
        echo json_encode($results);
        exit();
    }

    $results = true;

    try{
        $oMigraDelete = new Migra();
        $oMigraDelete->deleteByOrigem($idOrigem);
    }catch (Exception $e){
        $results = array(
            'error' => true,
            'msg' => 'Erro ao deletar o registro!' . $e->getMessage()
        );
        echo json_encode($results);
    }

    foreach($listCampos as $campo){
        $oMigra = new Migra();
        $idCampo = $campo;
        $oMigra->setIdCampo($idCampo);
        $oMigra->setIdOrigem($idOrigem);
        try{
            if ($oMigra ->insert()) {
                $results = true;
            } else {
                $results = false;
            }
        }catch (Exeception $e){
            $erro.=" | Houve um problema para gravar... verifique! |".$e->getMessage();
        }
    }

    if ($results) {
        $results = array(
            'error' => false,
            'msg' => 'Apontamentos salvos com sucesso!'
        );
    } else {
        $results = array(
            'error' => true,
            'msg' => 'Erro ao salvar os apontamentos! Verifique as informações!'
        );
    }

    echo json_encode($results);
?>