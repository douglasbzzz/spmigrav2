<?php
    session_start();
    require_once("../../model/Destino.php");

    $id = $_POST['txtId'];
    $nome = $_POST['txtNome'];

    if ($nome == null || $nome == "") {
        $results = array(
            'error' => true,
            'msg' => 'Nome inválido!'
        );
        echo json_encode($results);
        exit();
    }

    $oDestino = new Destino();
    $oDestino -> setNome(addslashes($nome));

    try{
        if ($id > 0 && $id <> "") {
            if ($oDestino ->update($id)) {
                $results = array(
                    'error' => false,
                    'msg' => 'Destino alterado com sucesso!'
                );
            } else {
                $results = array(
                    'error' => true,
                    'msg' => 'Erro ao alterar o Destino! Verifique as informações!'
                );
            }
        } else {
            if ($oDestino ->insert()) {
                $results = array(
                    'error' => false,
                    'msg' => 'Destino salvo com sucesso!'
                );
            } else {
                $results = array(
                    'error' => true,
                    'msg' => 'Erro ao salvar o Destino! Verifique as informações!'
                );
            }
        }
        echo json_encode($results);
    }catch (Exception $e){
        $results = array(
            'error' => true,
            'msg' => 'Erro ao salvar o Destino!' . $e->getMessage()
        );
        echo json_encode($results);
    }
