<?php

session_start();
require_once("../../model/Destino.php");

echo getDatatable();

function getDatatable() {
    $oDestino = new Destino();
    $str = "<table class='table table-hover'>";
    $str .= "<thead class='thead-light'>";
    $str .= "<tr>";
    $str .= "<th>Destinos</th>";
    $str .= "<th  style='width:  20%'>Ações</th>";
    $str .= "</tr>";
    $str .= "</thead>";
    foreach ($oDestino->findAll() as $key => $valor) {
        $str .=  "<tbody>";
        $str .= "<tr>";
        $str .= "<td>$valor->nome</td>";
        $str .= "<td>";
        $str .= "<button id='btnEditar' type='button' class='btn btn-outline-secondary mr-2' value='$valor->id' data-toggle='modal' data-target='#mdFormDestino'>";
        $str .= "<i class='fas fa-edit'></i>";
        $str .= "</button>";
        $str .= "<button id='btnExcluir' type='button' class='btn btn-outline-danger' value='$valor->id'>";
        $str .= "<i class='fas fa-trash-alt'></i>";
        $str .= "</button>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "</tbody>";
    }
    $str .= "</table>";
    return $str;
}