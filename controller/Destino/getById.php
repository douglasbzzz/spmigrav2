<?php
    session_start();
    require_once("../../model/Destino.php");

    $id = $_GET['id'];

    if ($id == null || $id == 0) {
        $results = array(
            'error' => true,
            'msg' => 'Registro inválido!'
        );
        echo json_encode($results);
        exit();
    }

    $destino = new Destino();
    $destinoEdit = $destino->findByID($id);

    echo json_encode($destinoEdit);
