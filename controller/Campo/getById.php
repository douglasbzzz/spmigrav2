<?php
    session_start();
    require_once("../../model/Campos.php");

    $id = $_GET['id'];

    if ($id == null || $id == 0) {
        $results = array(
            'error' => true,
            'msg' => 'Registro inválido!'
        );
        echo json_encode($results);
        exit();
    }

    $campo = new Campos();
    $campoEdit = $campo->findByID($id);

    echo json_encode($campoEdit);
