<?php
require_once("../../model/Campos.php");

$id = $_POST['txtId'];
$nome = $_POST['txtNomeCampo'];
$apelidoCampo = $_POST['txtApelidoCampo'];
$idDestino = $_POST['sDestino'];
$idModulo = $_POST['sModulo'];

if ($nome == null || $nome == "") {
    $results = array(
        'error' => true,
        'msg' => 'Nome inválido!'
    );
    echo json_encode($results);
    exit();
}

if ($apelidoCampo == null || $apelidoCampo == "") {
    $results = array(
        'error' => true,
        'msg' => 'Apelido inválido!'
    );
    echo json_encode($results);
    exit();
}

if($idDestino == 0){
    $results = array(
        'error' => true,
        'msg' => 'Destino selecionado inválido!'
    );
    echo json_encode($results);
    exit();
}

if($idModulo == 0){
    $results = array(
        'error' => true,
        'msg' => 'Módulo selecionado inválido!'
    );
    echo json_encode($results);
    exit();
}

$oCampo = new Campos();
$oCampo->setNomeCampo($nome);
$oCampo->setApelidoCampo($apelidoCampo);
$oCampo->setModulo($idModulo);

try{
    if ($id > 0 && $id <> "") {
        if ($oCampo->update($id)) {
            $results = array(
                'error' => false,
                'msg' => 'Campo alterado com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao alterar o Campo! Verifique as informações!'
            );
        }
    } else {
        if ($oCampo->insert()) {
            $results = array(
                'error' => false,
                'msg' => 'Campo salvo com sucesso!'
            );
        } else {
            $results = array(
                'error' => true,
                'msg' => 'Erro ao salvar o Campo! Verifique as informações!'
            );
        }
    }
    echo json_encode($results);
}catch (Exception $e){
    $results = array(
        'error' => true,
        'msg' => 'Erro ao salvar o Campo!' . $e->getMessage()
    );
    echo json_encode($results);
}