<?php
session_start();
require_once("../../model/Campos.php");

parse_str(file_get_contents("php://input"),$post_vars);
$id = $post_vars['id'];

if ($id > 0 && $id == "") {
    $results = array(
        'error' => true,
        'msg' => 'Erro ao deletar o registro!'
    );
    echo json_encode($results);
    exit();
}

try{
    $oCampos = new Campos();
    $oCampos->delete($id);
    $results = array(
        'error' => false,
        'msg' => 'Registro deletado com sucesso!'
    );
    echo json_encode($results);
}catch (Exception $e){
    $results = array(
        'error' => true,
        'msg' => 'Erro ao deletar o registro!' . $e->getMessage()
    );
    echo json_encode($results);
}
