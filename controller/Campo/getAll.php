<?php

session_start();
require_once("../../model/Campos.php");

$idDestino = $_GET['sDestinoFiltro'];
$idModulo = $_GET['sModuloFiltro'];

echo getDatatable($idDestino, $idModulo);

function getDatatable($prIdDestino, $prIdModulo) {
    $oCampos = new Campos();
    $str = "<table class='table table-hover'>";
    $str .= "<thead class='thead-light'>";
    $str .= "<tr>";
    $str .= "<th>Campo DB</th>";
    $str .= "<th>Apelido</th>";
    $str .= "<th>Destino</th>";
    $str .= "<th>Módulo</th>";
    $str .= "<th  style='width:  20%'>Ações</th>";
    $str .= "</tr>";
    $str .= "</thead>";
    foreach ($oCampos->retCampos($prIdDestino, $prIdModulo) as $key => $valor){
        $str .= "<tbody>";
        $str .= "<tr>";
        $str .= "<td>$valor->nomecampo</td>";
        $str .= "<td>$valor->apelido</td>";
        $str .= "<td>$valor->nomedestino</td>";
        $str .= "<td>$valor->nomemodulo</td>";
        $str .= "<td>";
        $str .= "<button id='btnEditar' type='button' class='btn btn-outline-secondary mr-2' value='$valor->id' data-toggle='modal' data-target='#mdFormCampos'>";
        $str .= "<i class='fas fa-edit'></i>";
        $str .= "</button>";
        $str .= "<button id='btnExcluir' type='button' class='btn btn-outline-danger' value='$valor->id'>";
        $str .= "<i class='fas fa-trash-alt'></i>";
        $str .= "</button>";
        $str .= "</td>";
        $str .= "</tr>";
        $str .= "</tbody>";
    }
    $str .= "</table>";
    return $str;
}