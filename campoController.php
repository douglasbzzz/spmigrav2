<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 24/06/2018
 * Time: 10:58
 */
require_once("model/Campos.php");

$erro = "cleiton";

if(isset($_POST['txtNomeCampo']) && strlen($_POST['txtNomeCampo'])<>0){
    $nomeCampo = addslashes($_POST['txtNomeCampo']);
}else{
    $erro .= " Informe um nome para o Sistema de Origem!";
}
if(isset($_POST['txtApelidoCampo']) && strlen($_POST['txtApelidoCampo'])<>0){
    $apelidoCampo = addslashes($_POST['txtApelidoCampo']);
}else{
    $erro .= " Informe um nome para o Fornecedor!";
}
if(isset($_POST['sModulo'])){
    $idModulo = $_POST['sModulo'];
}

$oCampo = new Campos();

if(isset($nomeCampo)){
    $oCampo->setNomeCampo($nomeCampo);
}
if(isset($apelidoCampo)){
    $oCampo->setApelidoCampo($apelidoCampo);
}
if(isset($idModulo)){
    $oCampo->setModulo($idModulo);
}

try{
    if($oCampo->insert()){
        header('location:campos.php?erro=2');
    }else{
        header('location:campos.php?erro=1');
    }

}catch (Exception $e){
    echo $erro.="Problema para gravar...";
}

?>