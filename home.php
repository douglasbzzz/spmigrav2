<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 13/06/2018
 * Time: 21:01
 */

session_start();

require_once("model/Origem.php");
require_once("model/Destino.php");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8"/>
    <title>SpMigrações</title>
    <link rel="stylesheet" href="assets/bootstrap.css"/>
    <link rel="stylesheet" href="assets/select2.min.css">
    <link rel="stylesheet" href="assets/geral.css"/>
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32x32.png">
</head>

<body>

<?php include "header.php"; ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 card shadow p-3 mb-5 bg-white rounded formulario">
            <form id="frmPesquisa" method="post">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Buscar Origens</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="card">
                            <div class="card-header" style="padding-bottom: 0px;">
                                <p>
                                    <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseFiltros" aria-expanded="false" aria-controls="collapseFiltros">
                                        Abrir Filtros
                                    </button>
                                </p>
                            </div>
                            <div class="collapse" id="collapseFiltros" style="padding: 10px;">
                                <select class="form-control select2" data-placeholder="Selecione o Sistema de Origem"
                                        name="sSistemaFiltro" id="sSistemaFiltro" required>
                                    <?php
                                    echo "<option value='0'>Selecione uma Destino...</option>";
                                    $oDestino = new Destino();
                                    foreach ($oDestino->findAll() as $key => $valor) {
                                        echo "<option value='$valor->id'>$valor->nome</option>";
                                    }
                                    ?>
                                </select>
                                <br/>
                                <br/>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="txtFornecedorFiltro" id="txtFornecedorFiltro" placeholder="Nome do Fornecedor" required/>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" name="txtFormatoBackupFiltro" id="txtFormatoBackupFiltro" placeholder="Formato ou extensão do Bakcup" required/>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-info" id="btnFiltrar" style="float: right">Filtrar</button>
                            </div>
                        </div>
                        <br/>
                        <select class="form-control select2" data-placeholder="Selecione o Sistema de Origem"
                                name="sSistema" id="sSistema" required>
                            <option value='0'>Selecione uma Destino...</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div id="info" class="col-md-4 card shadow p-3 mb-5 bg-white rounded formulario" >
            <h3>Informações Gerais</h3>
            <div id="infoOrigem">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="migr" class="card shadow p-3 mb-5 bg-white rounded formulario" hidden>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Campos Migrados</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div id="treeCamposMigrados">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--JS'S-->
<script type="text/javascript" src="assets/jquery-3.3.1.js"></script>
<script type="text/javascript" src="assets/bootstrap.js"></script>
<script src="assets/select2.full.min.js"></script>
<script>
    $(".select2").select2();

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#sSistema").on('change', function () {
            var idOrigem = this.options[this.selectedIndex].value;
            if (idOrigem > 0) {
                $("#migr").removeAttr("hidden");
                $("#info").removeAttr("hidden");
                $.ajax({
                    url: 'getPesquisa.php',
                    method: 'post',
                    data: $('#frmPesquisa').serialize(),
                    success: function (data) {
                        $('#treeCamposMigrados').html(data);
                    },
                    error: function (data) {
                        $('#treeCamposMigrados').html(data);
                    }
                });
                $.ajax({
                    url: 'buscarInformacoesOrigem.php',
                    method: 'post',
                    data: $('#frmPesquisa').serialize(),
                    success: function (data) {
                        $('#infoOrigem').html(data);
                    },
                    error: function (data) {
                        $('#infoOrigem').html(data);
                    }
                });
            } else {
                $("#migr").attr("hidden", true);
                $("#info").attr("hidden", true);
            }
        });
        $("#btnFiltrar").on('click', function () {
            $.ajax({
                url: 'preencheComboOrigem.php',
                method: 'post',
                data: $('#frmPesquisa').serialize(),
                success: function (data) {
                    $('#sSistema').html(data);
                },
                error: function (data) {
                    $('#sSistema').html(data);
                }
            });
        });
        $.ajax({
            url: 'preencheComboOrigem.php',
            method: 'post',
            data: $('#frmPesquisa').serialize(),
            success: function (data) {
                $('#sSistema').html(data);
            },
            error: function (data) {
                $('#sSistema').html(data);
            }
        });
    });

</script>
<!--FIM DOS JS'S-->
</body>

</html>
