<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 03/07/2018
 * Time: 22:04
 */

    session_start();
    require_once("model/Modulo.php");
    require_once("model/Campos.php");
    require_once("model/Migra.php");
    require_once("model/functions.php");

    $idOrigem = validaEntrada($_POST['sSistema']);

    echo resultadoPesquisa($idOrigem);

    function validaEntrada($pIdOrigem){
        if (isset($pIdOrigem) && $pIdOrigem > 0) {
            $pIdOrigem = $pIdOrigem;
        } else {
            $pIdOrigem = 0;
        }
        logBusca($_SESSION['idusuario'],$pIdOrigem);
        return $pIdOrigem;
    }

    function resultadoPesquisa($pIdOrigem)
    {
        $oModulo = new Modulo();
        $str = "";
        foreach ($oModulo->retModulosByOrigem($pIdOrigem) as $key => $value) {
            $count = 1;
            $str .= "<div>";
            $oMigra = new Migra();
            if (count($oMigra->retMigraByModulo($value->idmodulo, $pIdOrigem)) > 0) {
                $str .= "<ul>";
                $str .= "<li><spam><b>$value->nomemodulo</b></spam></li>";
                if (count($oMigra->retMigraByModulo($value->idmodulo, $pIdOrigem)) > 0) {
                    $str .= "<div id='alinhamento'>";
                    $str .= "<ul>";
                    foreach ($oMigra->retMigraByModulo($value->idmodulo, $pIdOrigem) as $k => $v) {
                        $str .= "<li>$v->campoFull</li>";
                        if ($count % 5 == 0) {
                            $str .= "</ul>";
                            $str .= "<ul>";
                        }
                        $count++;
                    }
                    $str .= "</ul>";
                    $str .= "</div>";
                }
                $str .= "</ul>";
            }
            $str .= "</div>";
        }
        return $str;
    }
?>