<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 03/09/2018
 * Time: 21:48
 */

    session_start();
    require_once("model/Sugestao.php");

    $oSugestao = new Sugestao();
    $cleiton = "";

    if($_POST['txtDescricao']<>"" && strlen($_POST['txtDescricao'])<>0){
        $oSugestao->setDescricao(trim($_POST['txtDescricao']));
    }else{
        $cleiton .=" | Preencha a descrição...";
    }

    if($_SESSION['idusuario']<>0){
        $oSugestao->setIdUsuario($_SESSION['idusuario']);
    }else{
        $cleiton .= " | Usuário não autenticado...";
    }

    if($_POST['sPrioridade'] <> 0 ){
        $oSugestao -> setPrioridade($_POST['sPrioridade']);
    }else{
        $cleiton .= " | Selecione um nível de prioridade...";
    }

    $oSugestao -> setSituacao(0); //pendente de resolução default

    try{
        if($oSugestao->insert()){
            header("location:suggest.php?erro=2");
        }else{
            header("location:suggest.php?erro=1");
        }
    }catch (PDOException $e){
        echo "Houve um problema para gravar... verifique..: ".$e->getMessage();
    }



?>