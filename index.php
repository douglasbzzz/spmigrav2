<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 13/06/2018
 * Time: 21:01
 */

require_once("model/Origem.php");
$erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8" />
        <title>SpMigrações</title>
        <link rel="stylesheet" href="assets/bootstrap.css" />
        <link rel="stylesheet" href="assets/select2.min.css">
        <link rel="stylesheet" href="assets/geral.css" />
        <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32x32.png">
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">SpMigrações</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        </nav>

        <div class="container">
            <form class="card shadow p-3 mb-5 bg-white rounded formulario col-md-4 offset-md-4" id="frmPesquisa" action="loginController.php" method="post">
                <div class="form-row">
                    <div class="form-group">
                        <img src="https://www.sponteweb.com.br/Browser/imgLogin/Logo-e-assinatura-Sponte.jpg">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 text-center">

                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="text" id="txtLogin" class="form-control" name="txtLogin" required placeholder="Login ou e-mail" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="password" id="txtPass" class="form-control" name="txtPass" required placeholder="Senha" />
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <input type="submit" name="btnEnvia" class="btn btn-outline-primary col-md-12" id="btnEnvia" value="Login" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <?php
                if ($erro == 1) {
                    echo '<font color="#FF0000">Usuário ou Senha não encontrados, Verifique!<br/></font>';
                } elseif ($erro == 2) {
                    echo '<font color="#008000">Departamento salvo!!<br/></font>';
                }
                ?>
                    </div>
                </div>
                <a href="user/user.php" style="color: #34333a; text-align: right;">Primeiro acesso? Crie seu usuário...</a>
        </div>
        </div>
        </form>
        <div id="migr" class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" hidden>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <h3>Campos Migrados</h3>
                </div>
            </div>
            <div class="form-row">
                <div id="result">
                </div>
            </div>
        </div>

        </div>
        <!--JS'S-->
        <script type="text/javascript" src="assets/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="assets/bootstrap.js"></script>
        <script src="assets/select2.full.min.js"></script>
        <script>
            //select2
            $(".select2").select2();

        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#sSistema").on('change', function() {
                    var idOrigem = this.options[this.selectedIndex].value;
                    if (idOrigem > 0) {
                        $("#migr").removeAttr("hidden");
                        $.ajax({
                            url: 'getPesquisa.php',
                            method: 'post',
                            data: $('#frmPesquisa').serialize(),
                            success: function(data) {
                                $('#result').html(data);
                            },
                            error: function(data) {
                                $('#result').html(data);
                            }
                        });
                    } else {
                        $("#migr").attr("hidden", true);
                    }
                });

            });

        </script>
        <!--FIM DOS JS'S-->
    </body>

    </html>
