<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 26/07/2018
 * Time: 22:27
 */

    require_once("../model/User.php");

    $oUser = new User();
    $cleiton = "";

    if($_POST['txtNome']<>"" && strlen($_POST['txtNome'])<>0){
        $oUser->setNome(addslashes($_POST['txtNome']));
    }else{
        $cleiton .= "Nome de Usuário Inválido. Verifique";
    }

    if($_POST['txtEmail']<>"" && strlen($_POST['txtEmail'])<>0){
        $oUser->setEmail($_POST['txtEmail']);
    }else{
        $cleiton .= "e-mail inválido. Verifique";
    }

    if($_POST['txtLogin'] <> "" && strlen($_POST['txtLogin'])<>0){
        $oUser->setLogin(addslashes($_POST['txtLogin']));
    }else{
        $cleiton .= "login inválido! verifique...";
    }

    if($_POST['txtPass'] <> "" && strlen($_POST['txtPass'])>4){
        $oUser->setSenha(sha1($_POST['txtPass']));
    }else{
        $cleiton .= "a senha precisa ter pelo menos 4 caracteres...";
    }

    if($_POST['sDpto']<>0){
        $oUser->setIdDpto($_POST['sDpto']);
    }else{
        $cleiton .= "dpto. inválido...";
    }

    try{
        if($oUser->insert()){
            header("location:user.php?erro=2");
        }else{
            header("location:user.php?erro=1");
        }
    }catch (Exception $e){
        $cleiton .= " |Houve um problema para gravar... verifique! |".$e->getMessage();
    }
?>
