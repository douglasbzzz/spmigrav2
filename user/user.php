<?php
    require_once("../model/Departamento.php");
    $erro = isset($_GET['erro']) ? $_GET['erro'] : 3;
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8" />
        <title>SpMigrações</title>
        <link rel="stylesheet" href="../assets/bootstrap.css" />
        <link rel="stylesheet" href="../assets/select2.min.css">
        <link rel="stylesheet" href="../assets/geral.css" />
        <link rel="icon" type="image/png" sizes="32x32" href="../assets/favicon-32x32.png">
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../index.php">SpMigrações</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
        </nav>
        <div class="container">
            <form class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" id="frmPesquisa" action="userController.php" method="post">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Crie seu Usuário</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <input type="text" id="txtNome" class="form-control" name="txtNome" required placeholder="Nome Completo" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-10">
                        <input type="email" id="txtEmail" class="form-control" name="txtEmail" required placeholder="E-mail" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="text" id="txtLogin" class="form-control" name="txtLogin" required placeholder="Login" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input type="password" id="txtPass" class="form-control" name="txtPass" required placeholder="Senha" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <select class="form-control select2" data-placeholder="Selecione o seu Dpto." name="sDpto" id="sDpto" required>
                            <?php
                                echo "<option value='0'>Selecione seu departamento...</option>";
                                $oDepto = new Departamento();
                                foreach ($oDepto->findAll() as $key => $valor) {
                                    echo "<option value='$valor->id'>$valor->nome</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-8">
                    <input type="submit" name="btnEnvia" class="btn btn-outline-primary" id="btnEnvia" value="Criar Usuário" />
                    <?php
                if ($erro == 1) {
                    echo '<font color="#FF0000">Problema para salvar o usuário. Verifique!<br/></font>';
                } elseif ($erro == 2) {
                    echo '<font color="#008000">Usuário Criado!<br/></font>';
                }
                ?>
                    </div>
                </div>
                    <a href="../index.php">Já possuí usuário? Logue no sistema...</a>
            </form>
            <div id="migr" class="card shadow p-3 mb-5 bg-white rounded formulario col-md-8 offset-md-2" hidden>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <h3>Campos Migrados</h3>
                    </div>
                </div>
                <div class="form-row">
                    <div id="result">
                    </div>
                </div>
            </div>

        </div>

        <!--JS'S-->
        <script type="text/javascript" src="assets/jquery-3.3.1.js"></script>
        <script type="text/javascript" src="assets/bootstrap.js"></script>
        <script src="assets/select2.full.min.js"></script>
        <script>
            //select2
            $(".select2").select2();

        </script>
    </body>

    </html>
